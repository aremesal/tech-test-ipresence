<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| TYPES of resources
|--------------------------------------------------------------------------
|
| The resource we will query for getting the quotes from.
| It's the classname for the implemented class in Libraries
|
*/
$config['quotes_source'] = 'QuoteResourceLocal';
$config['quotes_source_local_path'] = APPPATH . 'db/quotes.json';

/*
|--------------------------------------------------------------------------
| CACHE time
|--------------------------------------------------------------------------
|
| The max time until we regenerate cache for an author. In seconds
|
*/
$config['quotes_cache_time'] = 3600;

/*
|--------------------------------------------------------------------------
| CACHE configuration
|--------------------------------------------------------------------------
|
| For exercise purposes, using Filebase, only mandatory config is the folder location
|
*/
$config['quotes_cache_filebase'] = APPPATH . 'db/cachebd.json';