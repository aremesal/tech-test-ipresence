<?php

interface IQuoteSource
{
    public function getAllQuotes() : array;

    public function getQuotesFromAuthor(string $author) : array;
}