<?php

include_once APPPATH . 'libraries/interfaces/IQuoteSource.php';

class QuoteResourceLocal implements IQuoteSource
{
    private $localfile;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->localfile = $this->CI->config->item('quotes_source_local_path');
    }

    /**
     * getQuotes
     * Get the quote's objects from the local example file
     *
     * @return array
     */
    public function getAllQuotes() : array
    {
        if(!file_exists($this->localfile))
            throw new Exception ('File not found');

        return json_decode(file_get_contents($this->localfile))->quotes;
    }

    /**
     * getQuotesFromAuthor
     * Get quotes and filter according to the author.
     *
     * @param string $author
     * @return array
     */
    public function getQuotesFromAuthor(string $author) : array
    {
        // Transform requested author-id to local file format
        $requested_author = $this->idToWords($author);
        $quotes = $this->getAllQuotes();

        $filtered = array_filter($quotes, function($quote) use ($requested_author){
            return $quote->author == $requested_author;
        });

        // Return only strings with the quotes
        return array_map(function ($quotes) { return $quotes->quote; }, $filtered);
    }

    /**
     * idToWords
     * Transforms an author-id to our local file format. In our local file, author is stored as an actual name (example: "Steve Jobs")
     *
     * @param string $string
     * @param bool $capitalizeFirstCharacter
     * @return string
     */
    function idToWords(string $string) : string
    {
        return ucwords(str_replace('-', ' ', $string));
    }
}