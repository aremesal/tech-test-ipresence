<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'repositories/Quotes.php';

use chriskacerguis\RestServer\RestController;

class Api extends RestController {

    function __construct()
    {
        parent::__construct();
    }

    /**
     * shout_get()
     * Entry point for the API, receives the GET request, process and returns the response
     *
     */
    public function shout_get()
    {
        // Error control ******************************
        // We require an author
        if( empty($this->get('author')))
        {
            $this->response( [
                'status' => false,
                'message' => 'An author is required.'
            ], self::HTTP_NOT_ACCEPTABLE );
        }

        // If a limit is set, not more than 10 and not less than 1, and be num
        if($this->get('limit') !== NULL
            && (!is_numeric($this->get('limit'))
            || ($this->get('limit') > 10 || $this->get('limit') < 1) ) )
        {
            $this->response( [
                'status' => false,
                'message' => 'You requested an invalid number of quotes (max: 10)'
            ], self::HTTP_NOT_ACCEPTABLE );
        }
        // -- End error control ******************************

        // If not limit is set, return our own limit
        $limit = $this->get('limit') ?: MAX_QUOTES_ALLOWED;

        try {
            // Bring quotes from the configured repository
            $quotes_source = $this->config->item('quotes_source');
            $repo = new Quotes(new $quotes_source());
            $quotes = $repo->getAuthorFilteredQuotes($this->get( 'author' ), $limit);

            // Check if we have available quotes from requested author, or return a 404 instead
            if( count($quotes) < 1 )
            {
                $this->response( [
                    'status' => false,
                    'message' => "Sorry, we don't have any quote from requested author."
                ], self::HTTP_NOT_FOUND );
            }

            // Prepare requested number of formatted quotes
            // Restriction: if we have less quotes than the requested number, we just will return all our quotes from the author
            $cont = 0;
            $response = Array();
            foreach ($quotes as $quote)
            {
                if($cont++ >= $limit) {
                    break;
                } else {
                    $response[] = $this->formatQuoteUppercaseExclamation($quote);
                }
            }

            // Return quotes
            $this->response( $response, 200 );
        }
        catch (Exception $e)
        {
            $this->response( [
                'status'    => false,
                'message'   => $e->getMessage()
            ], 500 );
        }


    }

    /**
     * formatQuoteUppercaseExclamation
     * Formats the quote into uppercase and exclamation mark. If there're other punctuation marks at the end, strips them.
     *
     * @param string $quote
     * @return string
     */
    private function formatQuoteUppercaseExclamation(string $quote) : string
    {
        return strtoupper(rtrim($quote, '!?.,;:')) . '!';
    }

}