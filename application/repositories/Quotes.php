<?php

class Quotes
{
    private $source;
    private $CI;
    private $cache_time;
    private $db;

    public function __construct(IQuoteSource $source)
    {
        $this->CI =& get_instance();
        $this->source = $source;
        $this->cache_time = $this->CI->config->item('quotes_cache_time');

        // TODO: For exercise purposes using Filebase for portability (ideally would be a Redis or similar)
        $this->db = new \Filebase\Database([
            'dir' => $this->CI->config->item('quotes_cache_filebase')
        ]);
    }

    /**
     * getAllQuotesFromSource
     * Get all the quotes directly from resouce
     *
     * @return array
     */
    public function getAllQuotesFromSource() : array
    {
        // TODO: implement getAllQuotesFromSource()
    }

    /**
     * getAuthorFilteredQuotes
     * get quotes from requested author. If $not_cached  is set to TRUE will ignore cache and query the remote source
     *
     * @param string $author
     * @param int $limit
     * @param bool $not_cached
     * @return array
     */
    public function getAuthorFilteredQuotes(string $author, int $limit = MAX_QUOTES_ALLOWED,  bool $not_cached = FALSE) : array
    {
        if($not_cached)
        {
            // Get the quotes directly from the source
            return $this->source->getQuotesFromAuthor($author);
        }
        else
        {
            // Use our local cache
            return $this->getAuthorFilteredQuotesFromCache($author, $limit);
        }
    }

    /**
     * getAuthorFilteredQuotesFromCache
     * get quotes from requested author from our local cache. If quotes don't exist yet for the author, or are too old, get new quotes before returning them
     *
     * @param string $author
     * @return array
     */
    private function getAuthorFilteredQuotesFromCache(string $author, int $limit) : array
    {

        // Get quotes from local cache.
        $quotes = $this->db->get($author);

        // If no cached data from the author or it's too old, get quotes from source and add to cache
        if(!$this->db->has($author) || strtotime($quotes->updatedAt()) + $this->cache_time < time())
        {
            $this->updateAuthorCache($author);
        }

        return $this->db->get($author)->quotes ?: Array();
    }

    /**
     * updateAuthorCache
     * Updates (or creates) the quotes for an author in the local cache storage
     *
     * @param string $author
     * @return mixed
     */
    private function updateAuthorCache(string $author)
    {
        $quotes = $this->source->getQuotesFromAuthor($author);

        $item = $this->db->get($author);
        return $item->save([
            'author_id' => $author,
            'author'    => ucwords(str_replace('-', ' ', $author)),
            'quotes'     => $quotes
        ]);
    }
}