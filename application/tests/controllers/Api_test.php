<?php
/**
 * Part of ci-phpunit-test
 *
 * @author     Kenji Suzuki <https://github.com/kenjis>
 * @license    MIT License
 * @copyright  2015 Kenji Suzuki
 * @link       https://github.com/kenjis/ci-phpunit-test
 */

class Api_test extends TestCase
{
    private $client;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->CI =& get_instance();

        // Using Guzzle to emulate API requests
        $this->client = new \GuzzleHttp\Client(['http_errors' => false]);

    }

    public function test_shout_nonexistent_author()
    {
        $response = $this->client->get('http://dev.ipresence.com/shout/unknown-author');

        $this->assertEquals(404, $response->getStatusCode());

        $data = json_decode($response->getBody(true), true);
        $this->assertContains('Sorry', $data['message']);
    }

    public function test_shout_number_too_high()
    {
        $response = $this->client->get('http://dev.ipresence.com/shout/steve-jobs/25');

        $this->assertEquals(406, $response->getStatusCode());

        $data = json_decode($response->getBody(true), true);
        $this->assertContains('You requested an invalid number of quotes', $data['message']);
    }

    public function test_shout_invalid_author()
    {
        $response = $this->client->get('http://dev.ipresence.com/shout');

        $this->assertEquals(406, $response->getStatusCode());

        $data = json_decode($response->getBody(true), true);
        $this->assertContains('An author is required', $data['message']);
    }

    public function test_shout_valid_steve_jobs()
    {
        $response = $this->client->get('http://dev.ipresence.com/shout/steve-jobs');

        $this->assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getBody(true), true);
        $this->assertContains('YOUR TIME IS LIMITED, SO DON’T WASTE IT LIVING SOMEONE ELSE’S LIFE!', $data[0]);
        $this->assertEquals(2, count($data));
    }

    public function test_shout_valid_steve_jobs_limited()
    {
        $response = $this->client->get('http://dev.ipresence.com/shout/steve-jobs/1');

        $this->assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getBody(true), true);
        $this->assertContains('YOUR TIME IS LIMITED, SO DON’T WASTE IT LIVING SOMEONE ELSE’S LIFE!', $data[0]);
        $this->assertEquals(1, count($data));
    }
}