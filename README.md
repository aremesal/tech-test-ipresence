# iPresence Tech Test

## Álvaro Remesal Royo

### Tech stack

As one of the restrictions was to **not** use Laravel, I used CodeIgniter instead, so I don't need to put time building an MVC, routing, etc.

No front-end, just the API access. 

For the cache, in order to not restrict to any DB and for simplicity, I used Filebase - the cache access is encapsulated in a repository.

## Deployment

It should work out-of-the-box as CodeIgniter doesn't require anything uncommon. Just be sure to have phpunit and php-curl for the tests.

It's tested with PHP 7.2

It's needed to change the `base_url` in the `config/config.php` file to the URL used in the testing server. Be aware of the trailing slash.

Path `application/db/cachebd.json` must be writteable by the web server.

## Using

I built a REST API for querying quotes. The sintax for a request is:

`http://urltoquotes/shout/author[/limit]`

By example:

`http://dev.ipresence.com/shout/steve-jobs/2`

will return an array with two quotes from Steve Jobs.

It uses the testing `quotes.json` file. The code is ready to add/change to another provider and there is a non-functionality class for a different provider left here just for showing purposes.
